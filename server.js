let http = require('http');

console.log('First project');

let httpServer = http.createServer();



httpServer.on('request', function(req, res){
    console.log(req);
    res.writeHead(200, 'OK', { contentType: 'text/plain'});
    res.write('This is my response');
    res.end();
});
httpServer.listen(8888); 